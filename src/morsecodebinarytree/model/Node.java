/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morsecodebinarytree.model;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class Node {

    /**
     * Představuje odkaz na první ze dvou výchozího uzle
     */
    private Node nodeA;
    /**
     * Představuje odkaz na druhý ze dvou výchozího uzle
     */
    private Node nodeB;
    /**
     * Úroveň zanoření
    *
     */
    private int level;
    /**
     * Kód morseovky {-,.}
     */
    private String morseCode;
    /**
     * Alphabet character
     */
    private String alphabetChar;

    public Node getNodeA() {
        return nodeA;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    public Node getNodeB() {
        return nodeB;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getMorseCode() {
        return morseCode;
    }

    public void setMorseCode(String morseCode) {
        this.morseCode = morseCode;
    }

    public String getAlphabetChar() {
        return alphabetChar;
    }

    public void setAlphabetChar(String alphabetChar) {
        this.alphabetChar = alphabetChar;
    }

}
