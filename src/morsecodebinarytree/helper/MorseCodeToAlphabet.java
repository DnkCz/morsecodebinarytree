/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morsecodebinarytree.helper;

import java.util.HashMap;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class MorseCodeToAlphabet {
    public static final HashMap<String,String> MORSECODE_ALPHABET_HASHMAP = createMap();
    private static HashMap<String, String> createMap()
    {
        HashMap<String,String> myMap = new HashMap<>();
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH, "A");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT, "B");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT, "C");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT, "D");
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT, "E");       
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT , "F");       
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT , "G");       
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT,"H");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH, "CH");       
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT,"I");
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH,"J");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH,"K");
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT,"L");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH,"M");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT,"N");       
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH,"O");       
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT,"P");     
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH,"Q");     
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT,"R");             
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT,"S");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH,"T");
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH,"U");
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH,"V");
        myMap.put(MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH ,"W");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH,"X");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH,"Y");
        myMap.put(MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT,"Z");
        return myMap;
    }

    
}
