/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morsecodebinarytree.app;

import java.util.Iterator;
import java.util.Map;
import morsecodebinarytree.helper.MorseCodeConfigurator;
import morsecodebinarytree.helper.MorseCodeToAlphabet;
import morsecodebinarytree.model.Node;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */

/**
 * Generates binaryTree
 * Fills generated binaryTree structure with alphabet
 */
class MorseCodeTreeGenerator {

    protected void generateNodes(Node node) {

        if (node.getLevel() > 4) {
            return;
        }
        // nodeA creation
        node.setNodeA(new Node());
        node.getNodeA().setMorseCode(MorseCodeConfigurator.MORSECODE_DOT);
        node.getNodeA().setLevel(node.getLevel() + 1);

        // nodeB creation
        node.setNodeB(new Node());
        node.getNodeB().setMorseCode(MorseCodeConfigurator.MORSECODE_DASH);
        node.getNodeB().setLevel(node.getLevel() + 1);

        this.generateNodes(node.getNodeA());
        this.generateNodes(node.getNodeB());

    }

    protected void fillNodesWithAlphabet(Node rootNode) {
        Iterator iterator = MorseCodeToAlphabet.MORSECODE_ALPHABET_HASHMAP.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();
            fillNodes(rootNode, (String) pair.getKey(), (String) pair.getValue());
        }

    }

    protected void fillNodes(Node n, String morseCode, String alphabet) {
        // empty input
        if (n.getLevel() <= 0 && morseCode.isEmpty()) {
            return;
        }

        // We finished searching. Assign aplhabet and end.
        if (morseCode.isEmpty()) {
            n.setAlphabetChar(alphabet);
            return;
        }

        char firstChar = morseCode.charAt(0);
        // Strip searched string
        morseCode = morseCode.substring(1, morseCode.length());

        // dot or dash logic
        if (String.valueOf(firstChar).equals(MorseCodeConfigurator.MORSECODE_DOT)) {
            fillNodes(n.getNodeA(), morseCode, alphabet);
            return;
        }

        if (String.valueOf(firstChar).equals(MorseCodeConfigurator.MORSECODE_DASH)) {
            fillNodes(n.getNodeB(), morseCode, alphabet);
            return;
        }

        // wrong morsecode<->alphabet if end up here
        return;

    }

}
