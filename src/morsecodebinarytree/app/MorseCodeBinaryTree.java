/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morsecodebinarytree.app;

import morsecodebinarytree.helper.MorseCodeConfigurator;
import morsecodebinarytree.model.Node;


/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class MorseCodeBinaryTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // VYGENEROVÁNÍ STRUKTURY BINÁRNÍHO STROMU
        MorseCodeTreeGenerator morseCodeGenerator = new MorseCodeTreeGenerator();
        Node rootNode = new Node();
        rootNode.setLevel(0);
        rootNode.setMorseCode(null);
        rootNode.setAlphabetChar(null);
        morseCodeGenerator.generateNodes(rootNode);
        morseCodeGenerator.fillNodesWithAlphabet(rootNode);
        
        // TEST 
        MorseCodeComputer morseCodeComputer = new MorseCodeComputer();
        String searchedMorseCode = MorseCodeConfigurator.MORSECODE_DASH+MorseCodeConfigurator.MORSECODE_DOT+MorseCodeConfigurator.MORSECODE_DOT;
        String result = morseCodeComputer.getAlphabetForMorseCode(rootNode,searchedMorseCode);
        System.out.println("Morse code: "+searchedMorseCode);
        System.out.println("Alphabet: "+result);
        
    }
    
    
    
    
    
    
}
