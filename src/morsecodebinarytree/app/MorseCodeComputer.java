/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morsecodebinarytree.app;

import morsecodebinarytree.helper.MorseCodeConfigurator;
import morsecodebinarytree.model.Node;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class MorseCodeComputer {
    
    /*
    * Node n - root node of given binary tree structure
    * morseCode - morscode for which we are searching alphabet
    * @return alphabet or null if error.
    */
    protected String getAlphabetForMorseCode(Node n, String morseCode) {
        // empty input
        if (n.getLevel() <= 0 && morseCode.isEmpty()) {
            return null;
        }

        // We finished searching. Assign aplhabet and end.
        if (morseCode.isEmpty()) {
            return n.getAlphabetChar();
        }

        char firstChar = morseCode.charAt(0);
        // Strip searched string
        morseCode = morseCode.substring(1, morseCode.length());

        // dot or dash logic
        if (String.valueOf(firstChar).equals(MorseCodeConfigurator.MORSECODE_DOT)) {
           return getAlphabetForMorseCode(n.getNodeA(), morseCode);
            
        }

        if (String.valueOf(firstChar).equals(MorseCodeConfigurator.MORSECODE_DASH)) {
           return getAlphabetForMorseCode(n.getNodeB(), morseCode);
           
        }

        // wrong morsecode<->alphabet if end up here
       return null;
    }
    
}
